from flask import Flask, request, session, jsonify, url_for, Response
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import *
from sqlalchemy.sql.expression import func
from sqlalchemy import create_engine

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:Password@mysqlserver/expenses_database'
app.config['SECRET_KEY'] = "random string"
db = SQLAlchemy(app)
#engine =create_engine(mysql://root:Password@localhost/expenses_database)
#db.create_engine()


class Expenses(db.Model):

   id = db.Column('id', db.Integer, primary_key = True)
   name = db.Column(db.String(100))
   email = db.Column(db.String(100))
   category = db.Column(db.String(100)) 
   description = db.Column(db.String(100))
   link = db.Column(db.String(100))
   estimated_costs = db.Column(db.String(100))
   submit_date = db.Column(db.String(100))
   status = db.Column(db.String(100))
   decision_date = db.Column(db.String(100))



   def __init__(self, name, email, category, description, link, estimated_costs, submit_date, status, decision_date ):

        self.name = name
        self.email = email
        self.category = category
        self.description = description
        self.link = link
        self.estimated_costs = estimated_costs
        self.submit_date = submit_date
        self.status = status
        self.decision_date = decision_date

class CreateDB():
    def __init__(self):
                import sqlalchemy
                engine=sqlalchemy.create_engine('mysql://%s:%s@%s'%("root","Password","mysqlserver")) 
                engine.execute("CREATE DATABASE IF NOT EXISTS %s "%("expenses_database")) 


@app.route('/')
def hello():
   return jsonify({'message':'It works!'})

@app.route('/v1/expenses', methods=['POST'])
def addExpense():
        expense=request.get_json(force=true)
        name=expense['name']
        email=expense['email']
        category=expense['category']
        description=expense['description']
        link=expense['link']
        estimated_costs=expense['estimated_costs']
        submit_date=expense['submit_date']
        status=expense['status']
        decision_date=expense['decision_date']
        #expense = Expenses(request.json['name'], request.json['email'], request.json['category'], request.json['description'],request.json['link'], request.json['estimated_costs'], request.json['submit_date'], request.json['status'], request.json['decision_date'])
        expense=Expenses(name,email,category,description,link,estimated_costs,submit_date,status,decision_date)
        db.session.add(expense)
        db.session.commit()
        id_res = db.session.query(func.max(Expenses.id))
        result = Expenses.query.filter_by(id= id_res).first_or_404()
        result_r = {'id':result.id,'name':result.name,'email':result.email,'category':result.category,'description':result.description, 'link':result.link,'estimated_costs':result.estimated_costs,'submit_date':result.submit_date,'status':result.status,'decision_date':result.decision_date}
        resp= jsonify(result_r)
        resp.status_code=201
        return resp

@app.route('/v1/expenses/<string:id>', methods=['GET'])
def returnExpense(id):
        result = Expenses.query.filter_by(id= id).first_or_404()
        result_r = {'id':result.id,'name':result.name,'email':result.email,'category':result.category,'description':result.description, 'link':result.link,'estimated_costs':result.estimated_costs,'submit_date':result.submit_date,'status':result.status,'decision_date':result.decision_date}
        resp= jsonify(result_r)
        resp.status_code=200
        return resp


@app.route('/v1/expenses/<string:id>', methods=['PUT'])
def editExpense(id):
        #Response(staus=202)
        expense=request.get_json(force=true)
        estimated_costs=expense['estimated_costs']
        result=Expenses.query.filter_by(id=id).first_or_404()
        #result.estimated_costs= request.json['estimated_costs']
        result.estimated_costs=estimated_costs
        db.session.commit()
        resp=Response(status=202)
        return resp


@app.route('/v1/expenses/<string:id>', methods=['DELETE'])
def removeOne(id):
        result1 = Expenses.query.filter_by(id= id).first()
        if result1 is None:
            resp=Response(status=404)
        else:
            Expenses.query.filter_by(id= id).delete()
            db.session.commit()
            resp=Response(status=204)
        return resp
        
if __name__ == '__main__':
    CreateDB()
    db.create_all()
    app.run(debug=True, host='0.0.0.0')

    
